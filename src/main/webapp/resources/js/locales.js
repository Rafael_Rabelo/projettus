PrimeFaces.locales['pt_BR'] = {  
                closeText: 'Fechar',  
                prevText: 'Anterior',  
                nextText: 'Próximo',  
                currentText: 'Começo',  
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],  
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun', 'Jul','Ago','Set','Out','Nov','Dez'],  
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],  
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],  
                dayNamesMin: ['D','S','T','Q','Q','S','S'],  
                weekHeader: 'Semana',  
                firstDay: 1,  
                isRTL: false,  
                showMonthAfterYear: false,  
                yearSuffix: '',  
                timeOnlyTitle: 'Só Horas',  
                timeText: 'Horário',  
                hourText: 'Hora',  
                minuteText: 'Minuto',  
                secondText: 'Segundo',  
                currentText: 'Hoje',  
                ampm: false,  
                month: 'Mês',  
                week: 'Semana',  
                day: 'Dia',  
                allDayText : 'Todo Dia',
        	    amPmText: ['AM', 'PM'],
        	    closeButtonText: 'Fechar',
        	    nowButtonText: 'Hoje',
        	    deselectButtonText: 'Desmarcar',
        	    'MONTHS': ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        	    'MONTHS_SHORT': ['Jan','Fev','Mar','Abr','Mai','Jun', 'Jul','Ago','Set','Out','Nov','Dez'],
        	    'DAYS': ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        	    'DAYS_SHORT': ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
        	    'ZOOM_IN': "Aumentar",
        	    'ZOOM_OUT': "Diminuir",
        	    'MOVE_LEFT': "Mover a Esquerda",
        	    'MOVE_RIGHT': "Mover a Direita",
        	    'NEW': "Novo",
        	    'CREATE_NEW_EVENT': "Criar novo elemento"                	
            };