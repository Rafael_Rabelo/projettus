package br.com.projettus.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import br.com.projettus.dao.TarefaDAO;
import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Projeto;
import br.com.projettus.entity.Tarefa;
import br.com.projettus.util.HibernateUtil;

@ManagedBean(name="dashGerenciados")
@SessionScoped
public class DashGerenciadosBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2974854352515523731L;
	
	
	private ScheduleModel calendarioProj;
	private ScheduleModel calendarioAtiv;
	ProjetoBean projBean;
	private Projeto projeto;
	private Atividade atividade;
	private String prioridade;
	private String dataInicio;
	private String dataFim;
	private List<Tarefa> tarefas;
	
	
	@PostConstruct
    public void init(){
		projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
		tarefas = null;
		loadCalendarioProjeto();
		loadCalendarioAtividade();		
	}
	
	public void loadCalendarioProjeto() {
		calendarioProj = new DefaultScheduleModel();
				
		for(Projeto proj : projBean.getProjetoQueGerencio()){
			calendarioProj.addEvent(new DefaultScheduleEvent(proj.getNome(),proj.getDataInicio(),proj.getDataFim(),proj));
		}
	}	
	
	public void loadCalendarioAtividade() {
		calendarioAtiv = new DefaultScheduleModel();
		
		ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");		
		for(Projeto proj : projBean.getProjetoQueGerencio()){
			for(Equipe eqp : proj.getEquipes()) {
				for(Atividade atv : eqp.getAtividades()) {					
					calendarioAtiv.addEvent(new DefaultScheduleEvent(atv.getNome() +" - "+ proj.getNome() ,atv.getDataInicio(),atv.getDataFim(),atv));
				}
			}
		}
	}	
	
    public void calendarioProjSelect(SelectEvent selectEvent) {
    	ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();	
    	setProjeto((Projeto) event.getData());
    	
    	//Converte Data Inicio e Fim para String para serem exibidas
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
    	setDataInicio(sdf.format(projeto.getDataInicio()));
    	setDataFim(sdf.format(projeto.getDataFim()));
    }
    
    public void calendarioAtvSelect(SelectEvent selectEvent) {
    	ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();  
    	setAtividade((Atividade) event.getData());    	
    	switch (atividade.getPrioridade()) {
			case 1:{setPrioridade("Baixa"); break;}
			case 2:{setPrioridade("Moderada"); break;}
			case 3:{setPrioridade("Alta"); 	 break;}
			case 4:{setPrioridade("Urgente");  break;}
			default:setPrioridade("");
		}
    	
    	//Converte Data Inicio e Fim para String para serem exibidas
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
    	setDataInicio(sdf.format(atividade.getDataInicio()));
    	setDataFim(sdf.format(atividade.getDataFim()));
    	
    	tarefas = new ArrayList<Tarefa>(TarefaDAO.list(atividade));
    }    
	

// GETTERS E SETTERS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public ScheduleModel getCalendarioProj() {
		return calendarioProj;
	}

	public void setCalendarioProj(ScheduleModel calendarioProj) {
		this.calendarioProj = calendarioProj;
	}

	public ScheduleModel getCalendarioAtiv() {
		return calendarioAtiv;
	}

	public void setCalendarioAtiv(ScheduleModel calendarioAtiv) {
		this.calendarioAtiv = calendarioAtiv;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	public void setTarefas(List<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}	
	
	
}
