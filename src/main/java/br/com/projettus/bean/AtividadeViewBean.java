package br.com.projettus.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.projettus.dao.AtividadeDAO;
import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Tarefa;
import br.com.projettus.util.AtivPlus;


@ManagedBean(name="atividadeView")
@ViewScoped
public class AtividadeViewBean implements java.io.Serializable{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3933968035363664668L;
	
	private List<AtivPlus> atvPlus;	
	
	@PostConstruct
    public void init(){
		this.setAtvPlus(loadAtividades());
    }
    
    private List<AtivPlus> loadAtividades() {
    	
    	List<Atividade> atividades = AtividadeDAO.list();
    	List<AtivPlus> atPlusList = new ArrayList<AtivPlus>();
    	for(Atividade at: atividades) {
    		
    		AtivPlus atPlus = new AtivPlus();
    		    		
    		//Seta atividade
    		atPlus.setAtiv(at);   
    		
    		//Seta Cor Padrão
    		atPlus.setcolor("label-primary");  
    		
    		//Converte prioridade de int para Text
        	switch (at.getPrioridade()) {
    			case 1:{atPlus.setPrioridadeText("Baixa"); break;}
    			case 2:{atPlus.setPrioridadeText("Moderada"); break;}
    			case 3:{atPlus.setPrioridadeText("Alta"); 	 break;}
    			case 4:{atPlus.setPrioridadeText("Urgente");  break;}
    			default:atPlus.setPrioridadeText("");
    		}
        	
        	
        	//Converte Data Inicio e Fim para String para serem exibidas
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        	atPlus.setDataInicio(sdf.format(at.getDataInicio()));
        	atPlus.setDataFim(sdf.format(at.getDataFim()));
        	
        	
        	//Calcula Prazo
        	String atrasada = ".";
        	Date dtHj = new Date(System.currentTimeMillis());     	
        	long diff = (at.getDataFim().getTime() - dtHj.getTime())/60000;
        	
        	if(diff < 0) { 
        		atrasada = " atrasado.";
        		diff = diff * -1;
        		atPlus.setcolor("label-danger");
        	}    		
        	
        	if((diff/60) < 1) {
        		atPlus.setcolor("label-danger");
        		atPlus.setPrazo(diff + " min");
        	}else if((diff/1440) < 1) {
        		atPlus.setcolor("label-warning");
        		atPlus.setPrazo((diff/60) + " h");    		
        	}else {
        		atPlus.setPrazo((diff/1440) + " dias"+ atrasada);    		
    		}  
        	
        	//Calcular o Progresso da Tarefa
			int tarefaConcluida = 0;	
			Set<Tarefa> tarefas = at.getTarefas();
            for(Tarefa tr : tarefas) {
                if(tr.isStatus()) tarefaConcluida++;                
            }                    
            if(!tarefas.isEmpty()) {
            	atPlus.setProgresso(Integer.toString((tarefaConcluida * 100)/tarefas.size())+"%");
            }else {
            	atPlus.setProgresso("0%");
            }            
            if(atPlus.getProgresso().equals("100%")) atPlus.setcolor("label-success");
        	
        	
        	//Adiciona Atividade Personalizada a Lista
        	atPlusList.add(atPlus);
    	}    		
		return atPlusList;    	
    }
    
    public static AtivPlus loadAtividade(Atividade at) {
   		
    		AtivPlus atPlus = new AtivPlus();
    		    		
    		//Seta atividade
    		atPlus.setAtiv(at);   
    		
    		//Seta Cor Padrão
    		atPlus.setcolor("label-primary");  
    		
    		//Converte prioridade de int para Text
        	switch (at.getPrioridade()) {
    			case 1:{atPlus.setPrioridadeText("Baixa"); break;}
    			case 2:{atPlus.setPrioridadeText("Moderada"); break;}
    			case 3:{atPlus.setPrioridadeText("Alta"); 	 break;}
    			case 4:{atPlus.setPrioridadeText("Urgente");  break;}
    			default:atPlus.setPrioridadeText("");
    		}
        	
        	
        	//Converte Data Inicio e Fim para String para serem exibidas
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
        	atPlus.setDataInicio(sdf.format(at.getDataInicio()));
        	atPlus.setDataFim(sdf.format(at.getDataFim()));
        	
        	
        	//Calcula Prazo
        	String atrasada = ".";
        	Date dtHj = new Date(System.currentTimeMillis());     	
        	long diff = (at.getDataFim().getTime() - dtHj.getTime())/60000;
        	
        	if(diff < 0) { 
        		atrasada = " atrasado.";
        		diff = diff * -1;
        		atPlus.setcolor("label-danger");
        	}    		
        	
        	if((diff/60) < 1) {
        		atPlus.setcolor("label-danger");
        		atPlus.setPrazo(diff + " min");
        	}else if((diff/1440) < 1) {
        		atPlus.setcolor("label-warning");
        		atPlus.setPrazo((diff/60) + " h");    		
        	}else {
        		atPlus.setPrazo((diff/1440) + " dias"+ atrasada);    		
    		}  
        	
        	//Calcular o Progresso da Tarefa
			int tarefaConcluida = 0;	
			Set<Tarefa> tarefas = at.getTarefas();
            for(Tarefa tr : tarefas) {
                if(tr.isStatus()) tarefaConcluida++;                
            }                    
            if(!tarefas.isEmpty()) {
            	atPlus.setProgresso(Integer.toString((tarefaConcluida * 100)/tarefas.size())+"%");
            }else {
            	atPlus.setProgresso("0%");
            }            
            if(atPlus.getProgresso().equals("100%")) atPlus.setcolor("label-success");
    	    		
		return atPlus;    	
    }    

    
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++    

	public List<AtivPlus> getAtvPlus() {
		return atvPlus;
	}

	public void setAtvPlus(List<AtivPlus> atvPlus) {
		this.atvPlus = atvPlus;
	}

}
