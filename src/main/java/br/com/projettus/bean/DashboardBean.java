package br.com.projettus.bean;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

import br.com.projettus.dao.EquipeDAO;
import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Tarefa;
import br.com.projettus.util.HibernateUtil;


@ManagedBean(name="dashboard")
@SessionScoped
public class DashboardBean implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4333933606924427940L;
	private TimelineModel model;
    private Date dtInicial;  
    private Date dtFinal;	
    private List<Equipe> equipes;
    private int Total = 0, ProgressoDoProjeto = 0, AtividadesConcluidas = 0, AtividadesAndamento = 0, AtividadesAtrazo = 0;
	
	@PostConstruct
    public void init(){
		dashboardLoad();
		calcProgresso();
	}	
	
	public void dashboardLoad() {
		
		ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");				
		setDtInicial(projBean.getProjeto().getDataInicio());
		setDtFinal(projBean.getProjeto().getDataFim());

		this.setEquipes(EquipeDAO.list());
        
        model = new TimelineModel();  
        
        String[] prioridade = new String[] {"baixa", "moderada", "alta", "urgente"};
   
        for(Equipe equipe : equipes) {
            for (Atividade atv : equipe.getAtividades()) {  
                TimelineEvent event = new TimelineEvent(atv.getNome(), atv.getDataInicio(), atv.getDataFim(), false, atv.getEquipe().getNome(), prioridade[atv.getPrioridade()-1]);
                model.add(event);  
            }          	
        }
	}
	
	public void calcProgresso() {		
		Date dtHj = new Date(System.currentTimeMillis());
		
		for(Equipe equipe : equipes){
			Total += equipe.getAtividades().size();
			for(Atividade at : equipe.getAtividades()) {
				int tarefasConcluidas = 0;	
				Set<Tarefa> tarefas = at.getTarefas();
		        for(Tarefa tr : tarefas) {
		            if(tr.isStatus()) tarefasConcluidas++;                
		        }                    
		        if(!tarefas.isEmpty()) {
		        	ProgressoDoProjeto += (tarefasConcluidas * 100)/tarefas.size();
		        	if((tarefasConcluidas * 100)/tarefas.size() == 100) {
	        			setAtividadesConcluidas(getAtividadesConcluidas() + 1);
        			}else {
	    				if((dtHj.after(at.getDataInicio()) && dtHj.before(at.getDataFim())) || dtHj.equals(at.getDataInicio()) || dtHj.equals(at.getDataFim())) {
	    					setAtividadesAndamento(getAtividadesAndamento() + 1);
	    				}else {
	    					if(dtHj.after(at.getDataFim())) {
	    						setAtividadesAtrazo(getAtividadesAtrazo() + 1);
	    					}
	    				}
    				}		        	
	        	}else {
    				if((dtHj.after(at.getDataInicio()) && dtHj.before(at.getDataFim())) || dtHj.equals(at.getDataInicio()) || dtHj.equals(at.getDataFim())) {
    					setAtividadesAndamento(getAtividadesAndamento() + 1);
    				}else {
    					if(dtHj.after(at.getDataFim())) {
    						setAtividadesAtrazo(getAtividadesAtrazo() + 1);
    					}
    				}
    			}		        
			}
		}
		if(Total > 0) ProgressoDoProjeto = ProgressoDoProjeto / Total;
	}
	
	public void linkAtividade(Atividade atv) throws IOException{
		TarefaBean tb = new TarefaBean();
		tb.setAtivPlus(AtividadeViewBean.loadAtividade(atv));
		tb.openTarefas();
		FacesContext.getCurrentInstance().getExternalContext().redirect("tarefas.xhtml");		
	}
	
	
// GETTERS E SETTERS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	public TimelineModel getModel() {
		return model;
	}

	public void setModel(TimelineModel model) {
		this.model = model;
	}

	public Date getDtInicial() {
		return dtInicial;
	}

	public void setDtInicial(Date dtInicial) {
		this.dtInicial = dtInicial;
	}

	public Date getDtFinal() {
		return dtFinal;
	}

	public void setDtFinal(Date dtFinal) {
		this.dtFinal = dtFinal;
	}

	public List<Equipe> getEquipes() {
		return equipes;
	}

	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}

	public int getAtividadesConcluidas() {
		return AtividadesConcluidas;
	}

	public void setAtividadesConcluidas(int atividadesConcluidas) {
		AtividadesConcluidas = atividadesConcluidas;
	}

	public int getAtividadesAndamento() {
		return AtividadesAndamento;
	}

	public void setAtividadesAndamento(int atividadesAndamento) {
		AtividadesAndamento = atividadesAndamento;
	}

	public int getAtividadesAtrazo() {
		return AtividadesAtrazo;
	}

	public void setAtividadesAtrazo(int atividadesAtrazo) {
		AtividadesAtrazo = atividadesAtrazo;
	}

	public int getProgressoDoProjeto() {
		return ProgressoDoProjeto;
	}

	public void setProgressoDoProjeto(int progressoDoProjeto) {
		ProgressoDoProjeto = progressoDoProjeto;
	}

	public int getTotal() {
		return Total;
	}

	public void setTotal(int total) {
		Total = total;
	}	
	
}
