package br.com.projettus.bean;

import br.com.projettus.entity.Usuario;
import br.com.projettus.util.Criptografia;
import br.com.projettus.util.HibernateUtil;
import br.com.projettus.dao.UsuarioDAO;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.model.UploadedFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="usuario")
@SessionScoped
public class UsuarioBean implements java.io.Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3233700778565277346L;
	
	
	private Usuario usuario;
	private List<Usuario> UsuarioList;	
	private String nome;
	private String matricula;
	private String email;
	private String senha;
	private UploadedFile file;
	
	@PostConstruct
    public void init(){
    	this.file = null;    	
    }
   
    public String save() {
    	//SETAR PARÂMETROS
    	this.usuario = new Usuario();
    	this.usuario.setNome(nome);
    	this.usuario.setMatricula(matricula);
    	this.usuario.setEmail(email);
    	this.usuario.setSenha(senha);
    	this.usuario.setAtivo(true);    
    	
    	//UPLOAD DA FOTO    	
    	Path folder = Paths.get(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "resources\\img\\users");        
        try (InputStream input = file.getInputstream()) {
			if(!this.file.getFileName().equals("")) {
				Path filePath = Files.createTempFile(folder, this.usuario.getEmail(), ".png");			
				Files.copy(input, filePath, StandardCopyOption.REPLACE_EXISTING);
				this.usuario.setImg(filePath.getFileName().toString());
			}else {
				this.usuario.setImg("userpadrao.png");
			}
	        UsuarioDAO.saveOrUpdate(usuario);
	        this.usuario = new Usuario();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return "login";
    }    
    
    public String edit() {
    	
    	if(!this.file.getFileName().equals("")){
	    	//UPLOAD DA FOTO
    		Path folder = Paths.get(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "resources\\img\\users");
	        try (InputStream input = file.getInputstream()) {	        	
	        	Path filePath = Files.createTempFile(folder, this.usuario.getEmail(), ".png");
				Files.copy(input, filePath, StandardCopyOption.REPLACE_EXISTING);
				File file = new File(folder+"\\"+this.usuario.getImg());
				file.delete();
				this.usuario.setImg(filePath.getFileName().toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        UsuarioDAO.saveOrUpdate(usuario);
        
        //SALVAR USUARIO
        return "index_projs";
    }
    
    public String editSenha() {
        if(Criptografia.Comparar(Criptografia.Criptografar(this.getMatricula()), usuario.getSenha())) {    	
    		usuario.setSenha(this.getSenha());
    		UsuarioDAO.saveOrUpdate(usuario);
    		return "index_projs";
    	}else {
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha atual incorreta.", "Favor digitar novamente."));
    		return "edit_senha";
    	}        
    }    
    
    public String delete(){
    	UsuarioDAO.delete(usuario);
        return "login";
    }       	 

    public String login() throws IOException {    	 		
    	this.usuario = UsuarioDAO.buscarUsuario(email);
    	if(usuario == null) {    		
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login ou Senha Incorretos.", "Favor tentar novamente."));
    		return "login";
    	}else {
    		if(Criptografia.Comparar(Criptografia.Criptografar(senha), usuario.getSenha())){    			
                FacesContext.getCurrentInstance().getExternalContext().redirect("pags/index_projs.xhtml");
    			return "";
    		}else {
    			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login ou Senha Incorretos.", "Favor tentar novamente."));    			
    			return "login";
    		}
    	}    	
    }
    
    public String logout() throws IOException{
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.invalidate();
		this.usuario = null;
		FacesContext.getCurrentInstance().getExternalContext().redirect("../login.xhtml");
    	return "";
    }
    
    public void checkLogin() {
    	UsuarioBean usuarioBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");
    	if(usuarioBean.getUsuario() == null) {
    		try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("../login.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
 
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++++++    
    
	public List<Usuario> getUsuarioList() {
		return UsuarioList;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
		UsuarioList = usuarioList;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
    public static long getSerialversionuid() {
        return getSerialversionuid();
    }

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
