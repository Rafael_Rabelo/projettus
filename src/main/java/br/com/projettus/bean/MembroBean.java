package br.com.projettus.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import br.com.projettus.dao.MembroDAO;
import br.com.projettus.dao.UsuarioDAO;
import br.com.projettus.entity.Membro;
import br.com.projettus.entity.Usuario;
import br.com.projettus.util.EquipeMembros;

@ManagedBean(name="membro")
@SessionScoped
public class MembroBean implements java.io.Serializable{
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8423140182090641287L;
	private Membro membro;
	private String usuario;
	private List<Usuario> membros;
	private List<Usuario> usuarios;
	private List<String> complemento;
	private EquipeMembros em;
	private Usuario usuarioMembro;

	@PostConstruct
    public void init(){
        this.membro 	 = new Membro();        
        this.complemento = new ArrayList<String>();
        this.membros = new ArrayList<Usuario>();
        usuarios = UsuarioDAO.list();

    }
	
    public String save() {    
    	FacesContext facesContext = FacesContext.getCurrentInstance();
        UIViewRoot uiViewRoot = facesContext.getViewRoot();
        HtmlInputText inputText = (HtmlInputText) uiViewRoot.findComponent("equipe:equipes:membro");
        usuario = (String) inputText.getValue();
    	for(Usuario usu : usuarios) {
    		if(usu.getNome().equals(usuario)) {
    			if(!em.getMembros().contains(usu)) {
	    			em.getMembros().add(usu);
	    			membro.setUsuario(usu); 
	    			this.membros.add(usu);    			 
	    			break;
    			}else {
    				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN , "Erro", "O usuario(a) "+ usu.getNome() + " já está incluido(a) na equipe "+ em.getEquipe().getNome() +"." ));
    				break;
    			}
    		}
    	}  	 	
    	if(membro.getUsuario() != null) {
	    	membro.setAtivo(true);    
	    	membro.setEquipe(em.getEquipe());
	    	MembroDAO.saveOrUpdate(membro);    	
	    	this.membro = new Membro();
	    	usuario = "";
    	}
    	return "equipes";
    }	
    
	public String delete(){    	
    	MembroDAO.delete(MembroDAO.findMembro(usuarioMembro.getIdUsuario(), em.getEquipe().getIdEquipe()));
    	em.getMembros().remove(usuarioMembro);
    	if(!this.usuarios.contains(usuarioMembro)) {
    		usuarios.add(usuarioMembro);
    	}    	
        return "equipes";
    }
    
	public List<String> buscarUsuario(String text){
		
		this.complemento.clear();
		
    	for (Usuario usu: usuarios) {
    		if(usu.getNome().length() > text.length()) {
				if(usu.getNome().substring(0, text.length()).toLowerCase().equals(text.toLowerCase())) {
					this.complemento.add(usu.getNome());
				}			
    		}
		}
    	
    	if(complemento.isEmpty()) {
    		complemento.add("Nenhum usuário encontrado.");
    	}
    	
    	return this.complemento;    	
    }
    
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++    

	public Membro getMembro() {
		return membro;
	}

	public void setMembro(Membro membro) {
		this.membro = membro;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public EquipeMembros getEm() {
		return em;
	}

	public void setEm(EquipeMembros em) {
		this.em = em;
	}

	public Usuario getUsuarioMembro() {
		return usuarioMembro;
	}

	public void setUsuarioMembro(Usuario usuarioMembro) {
		this.usuarioMembro = usuarioMembro;
	}	
	
}
