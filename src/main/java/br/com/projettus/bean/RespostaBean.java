package br.com.projettus.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.projettus.dao.RespostaDAO;
import br.com.projettus.dao.TopicoDAO;
import br.com.projettus.entity.Resposta;
import br.com.projettus.util.Forum;

@ManagedBean(name = "resposta")
@SessionScoped
public class RespostaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3820132303616114257L;
	private Resposta resposta;
	private Forum forum;
	private List<Resposta> respostas;	

	@PostConstruct
	public void init() {		
		resposta = new Resposta();	
	}
	
	public String openRespostas() {		
		respostas = new ArrayList<Resposta>(RespostaDAO.list(forum.getTop()));				
					
		if(!forum.isJavi()) {			
			forum.getTop().setVisualizacoes((short) (forum.getTop().getVisualizacoes() + 1));
			TopicoDAO.saveOrUpdate(forum.getTop());
			forum.setJavi(true);
			TopicoBean topicoBean = (TopicoBean) br.com.projettus.util.HibernateUtil.RecuperarDaSessao("topico");
			int index = topicoBean.getForuns().indexOf(forum);
			topicoBean.getForuns().remove(index);
			topicoBean.getForuns().add(index, forum);
		}			
		
		return "respostas";
	}
	
	public String save() {
		if(resposta.getIdResposta() == null) {
			resposta.setTopico(this.forum.getTop());
			respostas.add(RespostaDAO.saveOrUpdate(resposta));
		}else {
			resposta.setEditada(true);
			RespostaDAO.saveOrUpdate(resposta);
		}		
		 
		resposta = new Resposta();
		return "respostas";
	}	
	
	public String delete() {		
		respostas.remove(RespostaDAO.delete(resposta)); 
		resposta = new Resposta();
		return "respostas";
	}

	// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++	
	
	public Resposta getResposta() {
		return resposta;
	}
	public void setResposta(Resposta resposta) {
		this.resposta = resposta;
	}
	public Forum getForum() {
		return forum;
	}
	public void setForum(Forum forum) {
		this.forum = forum;
	}

	public List<Resposta> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<Resposta> respostas) {
		this.respostas = respostas;
	}
	
}
