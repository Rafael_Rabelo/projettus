package br.com.projettus.bean;


import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.projettus.dao.MembroDAO;
import br.com.projettus.dao.TarefaDAO;
import br.com.projettus.entity.Tarefa;
import br.com.projettus.util.AtivPlus;
import br.com.projettus.util.HibernateUtil;

@ManagedBean(name="tarefa")
@SessionScoped
public class TarefaBean implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5937945097292643041L;
	
	private AtivPlus ativPlus;
	private Set<Tarefa> tarefas;
	
	@PostConstruct
    public void init(){
		
    }
	
    public String openTarefas(){ 
    	tarefas = ativPlus.getAtiv().getTarefas();
    	return "tarefas";
    }
	
	public String save(Tarefa tarefa) {
		tarefa.setAtividade(this.ativPlus.getAtiv());		
		TarefaDAO.saveOrUpdate(tarefa);		
		this.atualizaProgresso();		
		return "tarefas";		
    }
    
    public String delete(Tarefa tarefa){
    	TarefaDAO.delete(tarefa);
    	tarefas.remove(tarefa);
    	this.atualizaProgresso();    	
        return "tarefas";
    }
    
    public void checkTarefa(Tarefa tarefa, int idEquipe) {
//    	if(tarefa.isStatus()) {
//    		UsuarioBean usuarioBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");    	
//    		tarefa.setMembro(MembroDAO.findMembro(usuarioBean.getUsuario().getIdUsuario(), idEquipe));
//    	}else {
//    		tarefa.setMembro(null);
//    	}
    	TarefaDAO.saveOrUpdate(tarefa);
    	this.atualizaProgresso();
    }       
    
    public void addTarefa(){
    	Tarefa tr = new Tarefa();
    	tr.setDescricao("Nova tarefa");
    	tr.setStatus(false);
    	this.tarefas.add(tr);
    	tr.setAtividade(this.ativPlus.getAtiv());		
		TarefaDAO.saveOrUpdate(tr);		
		this.atualizaProgresso();		
    }
    
    private void atualizaProgresso(){
    	//Calcular o Progresso da Tarefa
		int tarefaConcluida = 0;        
        for(Tarefa tr : tarefas) {
            if(tr.isStatus()) tarefaConcluida++;                
        }                    
        if(!tarefas.isEmpty()) {
        	this.ativPlus.setProgresso(Integer.toString((tarefaConcluida * 100)/tarefas.size())+"%");
        }else {
        	this.ativPlus.setProgresso("0%");
        }
    }
	
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++	
	public Set<Tarefa> getTarefas() {
		return tarefas;
	}


	public void setTarefas(Set<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}

	public AtivPlus getAtivPlus() {
		return ativPlus;
	}

	public void setAtivPlus(AtivPlus ativPlus) {
		this.ativPlus = ativPlus;
	}	
}
