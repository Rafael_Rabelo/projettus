package br.com.projettus.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;

import br.com.projettus.dao.TopicoDAO;
import br.com.projettus.entity.Topico;
import br.com.projettus.util.Forum;

@ManagedBean(name="topico")

@SessionScoped
public class TopicoBean implements java.io.Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 517141329191075968L;
	private Topico topico;
	private List<Forum> foruns = null;

	
	@PostConstruct
    public void init(){
		this.topico = new Topico();		
		if(foruns == null) {
			foruns = new ArrayList<Forum>();
			for(Topico top : TopicoDAO.list()) {
				foruns.add(this.createForum(top));
			}
		}
    }
	
    public String save() { 
    	if(topico.getIdTopico() == null) {
    		TopicoDAO.saveOrUpdate(topico);
    		foruns.add(this.createForum(topico));
    	}else {
    		topico.setEditada(true);
    		TopicoDAO.saveOrUpdate(topico);
    	}
    	return "forum";
    }	
    
	public String delete(Forum forum){    	
		TopicoDAO.delete(forum.getTop());	
		foruns.remove(forum);
        return "forum";
    }

	public Forum createForum(Topico top) {
		Forum forum = new Forum();
		forum.setTop(top);
		forum.setCountMsg(top.getRespostas().size());
		forum.setUser(top.getMembro().getUsuario().getNome());
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");		
		forum.setData(formato.format(top.getDataCriacao()));	
		return forum;
	}

// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++    

	public Topico getTopico() {
		return topico;
	}

	public void setTopico(Topico topico) {
		this.topico = topico;
	}

	public List<Forum> getForuns() {
		return foruns;
	}

	public void setForuns(List<Forum> foruns) {
		this.foruns = foruns;
	}
		
}
