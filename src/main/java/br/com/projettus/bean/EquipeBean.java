package br.com.projettus.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import br.com.projettus.dao.EquipeDAO;
import br.com.projettus.dao.MembroDAO;
import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Membro;
import br.com.projettus.entity.Usuario;
import br.com.projettus.util.EquipeMembros;

@ManagedBean(name="equipe")
@SessionScoped
public class EquipeBean implements java.io.Serializable{
	
	/**
	 *
	 */
	private static final long serialVersionUID = -8423140182090641287L;
	private Equipe equipe;
	private List<Usuario> membros;
	private List<Equipe> equipes;
	private List<EquipeMembros> equipeMembros;

	@PostConstruct
    public void init(){
        this.equipe 	 = new Equipe();
        this.equipeMembros = new ArrayList<EquipeMembros>();        
        this.equipes = new ArrayList<Equipe>(EquipeDAO.list());
        
        for(Equipe eq: equipes){
        	EquipeMembros em = new EquipeMembros();
        	membros = new ArrayList<Usuario>();
        	em.setEquipe(eq);	      
        	for(Membro m : MembroDAO.list(eq.getIdEquipe())) {
        		membros.add(m.getUsuario());        		
        	}   
        	em.setMembros(membros);
        	equipeMembros.add(em);    	
        }
    }
    
	public String save() {    	
    	EquipeDAO.saveOrUpdate(equipe);
		EquipeMembros em = new EquipeMembros();
		em.setEquipe(equipe);
		em.setMembros(new ArrayList<Usuario>());
		equipeMembros.add(em);
		this.equipe = new Equipe();
        return "equipes";
    }
	
    public String delete(){
    	EquipeDAO.delete(equipe);
    	for(EquipeMembros eqm : equipeMembros) {
    		if(eqm.getEquipe().equals(equipe)) {
    			equipeMembros.remove(eqm);
    			equipe = new Equipe();
    			break;
    		}
    	}
        return "equipes";
    }
    
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++    
	public List<Usuario> getMembros() {
		return membros;
	}

	public void setMembros(List<Usuario> membros) {
		this.membros = membros;
	}

	public List<Equipe> getEquipes() {
		return equipes;
	}

	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}
	
	public List<EquipeMembros> getEquipeMembros() {
		return equipeMembros;
	}

	public void setEquipeMembros(List<EquipeMembros> equipeMembros) {
		this.equipeMembros = equipeMembros;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
}
