package br.com.projettus.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import br.com.projettus.dao.ProjetoDAO;
import br.com.projettus.entity.Projeto;
import br.com.projettus.util.HibernateUtil;

@ManagedBean(name="projeto")
@SessionScoped
public class ProjetoBean implements java.io.Serializable{	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1913799245701571432L;
	private Projeto projeto;	
	private List<Projeto> projetoQueGerencio;
	private List<Projeto> projetoQueParticipo;	

	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();	
   
	@PostConstruct
    public void init(){
		UsuarioBean usu = new UsuarioBean(); usu.checkLogin();
    	List<Object[]> projetosIds = ProjetoDAO.list();
    	this.projetoQueGerencio = new ArrayList<Projeto>();
    	this.projetoQueParticipo = new ArrayList<Projeto>();    	
    	for(Object[] obj: projetosIds) {
			if((int) obj[1] == 0) {
				this.projetoQueGerencio.add(em.find(Projeto.class, obj[0]));
			}else {
				this.projetoQueParticipo.add(em.find(Projeto.class, obj[0]));
			}   		
        }
    }
    
    public String save() {
    	
    	Boolean edicao;
    	if(projeto.getIdProjeto() == null) {
    		edicao = false; 
    	}else {
    		edicao = true; 
    	}

    	if(ProjetoDAO.saveOrUpdate(projeto)) {    		
        	if(!edicao) {
        		this.projetoQueGerencio.add(projeto);
        	}
        	return "index_projs";
    	}else {
    		return "cad_projeto";
    	}
    }
    
    public String delete(Projeto projeto){
    	ProjetoDAO.delete(projeto);
    	this.projetoQueGerencio.clear();
    	this.init();
        return "index_projs";
    }
    
    public String edit(){
    	this.setProjeto(projeto);
    	return "cad_projeto";
    }    
    
    public String linkarProjeto() {
    	this.setProjeto(projeto);
    	return "dashboard";
    }
    
    public String novoProjeto() {
    	this.projeto = new Projeto();
    	return "cad_projeto";
    }

//   GETTERS e SETTERS ++++++++++++++++++++++++++++++++++++++++++   
	
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
    public static long getSerialversionuid() {
        return getSerialversionuid();
    }

	public List<Projeto> getProjetoQueGerencio() {
		return projetoQueGerencio;
	}

	public void setProjetoQueGerencio(List<Projeto> projetoQueGerencio) {
		this.projetoQueGerencio = projetoQueGerencio;
	}

	public List<Projeto> getProjetoQueParticipo() {
		return projetoQueParticipo;
	}

	public void setProjetoQueParticipo(List<Projeto> projetoQueParticipo) {
		this.projetoQueParticipo = projetoQueParticipo;
	}
}
