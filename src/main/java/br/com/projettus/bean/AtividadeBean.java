package br.com.projettus.bean;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.projettus.dao.AtividadeDAO;
import br.com.projettus.dao.EquipeDAO;
import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Projeto;
import br.com.projettus.util.HibernateUtil;


@ManagedBean(name="atividade")
@SessionScoped
public class AtividadeBean implements java.io.Serializable{	

	private static final long serialVersionUID = 3933968035363664668L;
	
	private Atividade atividade;
	private int IdEquipe;
	private List<Equipe> equipes;
	private Projeto projeto;
	
	@PostConstruct
    public void init(){		
		this.setEquipes(EquipeDAO.list());
		ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
		setProjeto(projBean.getProjeto());
    }
    
	public String save() {		
		atividade.setEquipe(AtividadeDAO.getEquipe(IdEquipe));
		if(AtividadeDAO.saveOrUpdate(atividade)) {
			DashboardBean dashBean = new DashboardBean();
			dashBean.dashboardLoad();
			dashBean.calcProgresso();
			return "atividades";
		}else {
			return "cad_atividade";
		}
    }
    
    public String delete(){
    	AtividadeDAO.delete(atividade);
        return "atividades.xhtml";
    }
    
    public String edit(){
    	this.setAtividade(atividade);    	
    	this.setIdEquipe(atividade.getEquipe().getIdEquipe());
    	return "cad_atividade";
    }
    
    public String newAtividade(){
    	this.atividade= new Atividade();
    	this.setIdEquipe(0);
    	return "cad_atividade";
    }

    
// GETTERS E SETTERS +++++++++++++++++++++++++++++++++++    

	public int getIdEquipe() {
		return IdEquipe;
	}

	public void setIdEquipe(int idEquipe) {
		IdEquipe = idEquipe;
	}
	
	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

	public List<Equipe> getEquipes() {
		return equipes;
	}

	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}		
}
