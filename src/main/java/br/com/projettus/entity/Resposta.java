package br.com.projettus.entity;
// Generated 21/08/2017 09:42:25 by Hibernate Tools 5.2.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Resposta generated by hbm2java
 */
@Entity
@Table(name = "resposta", catalog = "projettusdb")
public class Resposta implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2186768214623438178L;
	private Integer idResposta;
	private Membro membro;
	private Topico topico;
	private String resposta;
	private Date dataCriacao;
	private boolean editada;

	public Resposta() {
	}

	public Resposta(Membro membro, Topico topico, String resposta, Date dataCriacao, boolean editada) {
		this.membro = membro;
		this.topico = topico;
		this.resposta = resposta;
		this.dataCriacao = dataCriacao;
		this.editada = editada;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idResposta", unique = true, nullable = false)
	public Integer getIdResposta() {
		return this.idResposta;
	}

	public void setIdResposta(Integer idResposta) {
		this.idResposta = idResposta;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMembro", nullable = false)
	public Membro getMembro() {
		return this.membro;
	}

	public void setMembro(Membro membro) {
		this.membro = membro;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTopicos", nullable = false)
	public Topico getTopico() {
		return this.topico;
	}

	public void setTopico(Topico topico) {
		this.topico = topico;
	}

	@Column(name = "resposta", nullable = false, length = 65535)
	public String getResposta() {
		return this.resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dataCriacao", nullable = false, length = 19)
	public Date getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	
	@Column(name = "editada", nullable = false)
	public boolean isEditada() {
		return editada;
	}

	public void setEditada(boolean editada) {
		this.editada = editada;
	}	

}
