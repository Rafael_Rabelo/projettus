package br.com.projettus.dao;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.projettus.entity.Membro;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class MembroDAO {
	
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();	
	
    public static void saveOrUpdate(Membro membro) {     	        
        try {
        	em.getTransaction().begin();
            em.persist(membro);
            em.getTransaction().commit();
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
        }
    }
    public static void delete(Membro membro) {        
        try {            
            Membro m = em.find(Membro.class, membro.getIdMembro());
            if (m == null) {
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir membro " + membro.getUsuario().getNome()));
			}else {
				if((membro.getEquipe().getCodigo() == 0) && (membro.getEquipe().getMembros().size() == 1)) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", "A equipe de administradores do projeto não pode ficar sem nenhum membro."));
				}else {
					em.getTransaction().begin();
					em.remove(m);
		            em.getTransaction().commit();
	            }	            
			}            
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir membro " + membro.getUsuario().getNome()));
            em.getTransaction().rollback();
        }
    }         
    
    public static Membro findMembro(int idUsuario, int idEquipe) {
        Query query = em.createQuery("FROM Membro WHERE idEquipe = " + idEquipe + " AND idUsuario = " + idUsuario);            
        return (Membro) query.getResultList().get(0);
    }
    
	@SuppressWarnings("unchecked")
	public static List<Membro> list(int idEquipe) {
        try {
        	return (List<Membro>) Consultas.buscaPersonalizada("FROM Membro as membro WHERE membro.equipe.idEquipe = " + idEquipe,em);        	
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }        
    }  
}
