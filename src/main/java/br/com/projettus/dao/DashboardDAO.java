package br.com.projettus.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.projettus.bean.ProjetoBean;
import br.com.projettus.bean.UsuarioBean;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class DashboardDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	
	@SuppressWarnings("unchecked")
	public static boolean isGerentes() {
		ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
		UsuarioBean usuBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");
		String query =
				"SELECT idUsuario FROM projettusdb.membro as membro INNER JOIN " + 
				"(SELECT idEquipe FROM projettusdb.equipe WHERE idProjeto = "+ projBean.getProjeto().getIdProjeto() +" AND codigo = 0) as equipe " + 
				"ON membro.idEquipe = equipe.idEquipe";
		for(int id: (List<Integer>) Consultas.buscaSQL(query, em)) {
			if(id == usuBean.getUsuario().getIdUsuario())
				return true;		
		}
		return false;		
	}
}
