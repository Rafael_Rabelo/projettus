package br.com.projettus.dao;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.bean.UsuarioBean;
import br.com.projettus.entity.Membro;
import br.com.projettus.entity.Resposta;
import br.com.projettus.entity.Topico;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class RespostaDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	
	
    public static Resposta saveOrUpdate(Resposta resposta) {     	        
        try {        	
    		em.getTransaction().begin();
    		
        	if(resposta.getIdResposta() == null) {
        		
            	UsuarioBean usu = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario"); 
            	@SuppressWarnings("unchecked")
    			List<Membro> membros = (List<Membro>) Consultas.buscaPersonalizada("FROM Membro as membro WHERE membro.usuario.idUsuario = " + usu.getUsuario().getIdUsuario(),em);
            	resposta.setMembro(membros.get(0));
            	
            	resposta.setDataCriacao(new Date());            	
            	
        		em.persist(resposta);
    		}else {
    			em.merge(resposta);
    		}
        	
            em.getTransaction().commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Resposta salva com sucesso!"));
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao cadastrar Resposta "));
        	System.out.println(e);
            em.getTransaction().rollback();
        }
        return resposta;
    }
    public static Resposta delete(Resposta resposta) {         	
        try {   
			em.getTransaction().begin();
			em.remove(resposta);
            em.getTransaction().commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Resposta excluída com sucesso!"));
            return resposta;
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir resposta "));
        }        
        return null;
    }    
    
	@SuppressWarnings("unchecked")
	public static List<Resposta> list(Topico topico) {
		em.clear();
	    try {        
        	return (List<Resposta>) Consultas.buscaPersonalizada("FROM Resposta as resp WHERE resp.topico.idTopico = "+ topico.getIdTopico() ,em);   
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;        
    }  
}
