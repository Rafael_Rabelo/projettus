package br.com.projettus.dao;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.projettus.entity.Usuario;
import br.com.projettus.util.*;


public class UsuarioDAO {
	
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	public static Usuario usuario = new Usuario();
	
    public static void saveOrUpdate(Usuario user) {     	        
        try {
        	usuario = user; 
        	usuario.setSenha(Criptografia.Criptografar(usuario.getSenha()));        	
            if (user.getIdUsuario() == null) {
            	if(buscarUsuario(usuario.getEmail()) == null) {
            		em.getTransaction().begin();
	                em.persist(usuario);
	                em.getTransaction().commit();
	            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Usuario(a) "+ usuario.getNome()+" cadastrado com sucesso!"));	            	
            	}else {
                	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Email já cadastrado no sistema!"));
                	return;
                }
            }else {
            	em.getTransaction().begin();
                em.merge(usuario);
                em.getTransaction().commit();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Usuario(a) "+ usuario.getNome()+" alterado com sucesso!"));
            }            
            
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
        }
    }
    public static void delete(Usuario user) {        
        try {
        	usuario = user; 
            em.getTransaction().begin();
            Usuario usu = em.find(Usuario.class, usuario.getIdUsuario());
            em.remove(usu);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }    
    
    @SuppressWarnings("unchecked")
	public static List<Usuario> list() {
        List<Usuario> result = null;
        try {        	
            Query query = em.createQuery("FROM Usuario");            
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public static Usuario buscarUsuario(String email) {      	
    	Query query = em.createQuery("FROM Usuario as usu WHERE usu.email = :emailParam");
    	query.setParameter("emailParam", email);    	
    	@SuppressWarnings("unchecked")
    	List<Usuario> results = query.getResultList();

    	if(results.isEmpty()){
    	    return null;
    	} else {
    	    return results.get(0);
    	}
        
    }    
    
    public static List<Usuario> listarUsuarios(String selectQuery) {   
    	Query query = em.createQuery(selectQuery);
    	query.setParameter("emailParam", usuario.getEmail());
    	query.setParameter("nomeParam", usuario.getNome());
    	@SuppressWarnings("unchecked")
    	List<Usuario> results = query.getResultList();

    	if(results.isEmpty()){
    	    return null;
    	} else {
    	    return results;
    	}
        
    }
}
