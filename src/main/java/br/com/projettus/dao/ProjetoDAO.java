package br.com.projettus.dao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.bean.UsuarioBean;
import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Membro;
import br.com.projettus.entity.Projeto;
import br.com.projettus.util.*;


public class ProjetoDAO {
	
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	public static Projeto projeto = new Projeto();
	
    public static boolean saveOrUpdate(Projeto projeto) {     	        
        try {
        	em.getTransaction().begin();
    		if(projeto.getDataInicio().after(projeto.getDataFim()))
    		{   
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "A data inicial do projeto não pode ser após a data fim. "));
        		return false;
    		}
        	if(projeto.getIdProjeto() == null) {       		
     		
        		em.persist(projeto);
        		em.getTransaction().commit();
        		
            	//Cadastrar Equipe Admin
            	Equipe equipe = new Equipe();
            	equipe.setCodigo(0); 
            	equipe.setNome("Gerente(s) do Projeto");
            	equipe.setProjeto(projeto);
            	EquipeDAO.saveOrUpdate(equipe);
            	
            	//Cadastrar Membro Admin
            	Membro membro = new Membro();
            	membro.setEquipe(equipe);
            	membro.setAtivo(true);
            	UsuarioBean usuarioBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");            	
            	membro.setUsuario(usuarioBean.getUsuario());
            	MembroDAO.saveOrUpdate(membro);
            	
        	}else {
        		em.merge(projeto);
        		em.getTransaction().commit();   
        	}                       
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Projeto salvo com sucesso!"));            
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao cadastrar projeto. "));            
        }
        return true;
    }
    public static void delete(Projeto projeto) {        
        try {    
            Projeto proj = em.find(Projeto.class, projeto.getIdProjeto());
			em.getTransaction().begin();
			em.remove(proj);
            em.getTransaction().commit();	
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Projeto "+ projeto.getNome()+" excluído com sucesso!"));            
        } catch (Exception e) {
            em.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir projeto "+ projeto.getNome()));
        }    
    	
    }        
    
	@SuppressWarnings({ "unchecked" })
	public static List<Object[]> list() {	
		try {
        	List<Object[]> projetosIds = new ArrayList<Object[]>();
        	UsuarioBean usuarioBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");
        	projetosIds = (List<Object[]>) Consultas.buscaSQL("SELECT DISTINCT equipe.idProjeto, equipe.codigo FROM projettusdb.equipe as equipe " + 
											        		  "RIGHT JOIN (SELECT membro.idEquipe FROM projettusdb.membro where idUsuario = "+ usuarioBean.getUsuario().getIdUsuario() +") as membro " + 
											        		  "ON equipe.idEquipe = membro.idEquipe GROUP BY equipe.idProjeto",em);           
        	return projetosIds;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }  
}

