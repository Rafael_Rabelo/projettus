package br.com.projettus.dao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.bean.ProjetoBean;
import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Equipe;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class AtividadeDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	
    public static boolean saveOrUpdate(Atividade atividade) {     	        
        try {        	
        	ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
    		if(projBean.getProjeto().getDataInicio().after(atividade.getDataInicio()))
    		{   
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "A data inicial da atividade não pode ser antes da data inicial do projeto."));
        		return false;
    		}
    		
    		if(projBean.getProjeto().getDataFim().before(atividade.getDataFim()))
    		{   
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "A data final da atividade não pode depois da data final do projeto."));
        		return false;
    		}
    		
    		if(atividade.getDataInicio().after(atividade.getDataFim())) {
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "A data inicial da atividade não pode ser após a data final."));
        		return false;
    		}
    		
    		em.getTransaction().begin();
        	if(atividade.getIdAtividade() == null) {
        		em.persist(atividade);
    		}else {
    			em.merge(atividade);
    		}
        	
            em.getTransaction().commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade "+ atividade.getNome()+" salva com sucesso!"));
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao cadastrar atividade "));
        	System.out.println(e);
            em.getTransaction().rollback();
        }
//        em.close();
        return true;
    }
    public static void delete(Atividade atividade) {         	
        try {   
			em.getTransaction().begin();
			em.remove(atividade);
            em.getTransaction().commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade "+ atividade.getNome()+" excluída com sucesso!"));
			           
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir atividade "+ atividade.getNome()));
        }
    }    
    
    public static Equipe getEquipe(int id) {
    	return em.find(Equipe.class, id);
    }
    
	@SuppressWarnings("unchecked")
	public static List<Atividade> list() {
        List<Atividade> result = new ArrayList<Atividade>();        
    	for(Equipe eqp : EquipeDAO.list()) {
            try {        
            	result.addAll((List<Atividade>) Consultas.buscaPersonalizada("FROM Atividade as atv WHERE atv.equipe.idEquipe = "+eqp.getIdEquipe(),em));   
            } catch (Exception e) {
                e.printStackTrace();
            }		
    	}    	
        return result;
    }  
}
