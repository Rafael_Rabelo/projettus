package br.com.projettus.dao;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.entity.Atividade;
import br.com.projettus.entity.Tarefa;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class TarefaDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	
    public static void saveOrUpdate(Tarefa tarefa) {     	        
        try {        	
        	em.getTransaction().begin();
        	if(tarefa.getIdTarefa() == null) {
        		em.persist(tarefa);
        	}else {
        		em.merge(tarefa);
        	}
            
            em.getTransaction().commit();
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Tarefa salva com sucesso!"));
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao salvar tarefa"));
        	System.out.println(e);
            em.getTransaction().rollback();
        }
    }
    public static void delete(Tarefa tar) {
    	Tarefa tarefa = em.find(Tarefa.class, tar.getIdTarefa());
        try { 
			em.getTransaction().begin();
			em.remove(tarefa);
            em.getTransaction().commit();
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
        }
    }    
    
	@SuppressWarnings("unchecked")
	public static List<Tarefa> list(Atividade atividade) {
        List<Tarefa> result = null;
        try {
        	return (List<Tarefa>) Consultas.buscaPersonalizada("FROM Tarefa as tarefa WHERE tarefa.atividade.idAtividade = " + atividade.getIdAtividade(),em);         
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }  
}
