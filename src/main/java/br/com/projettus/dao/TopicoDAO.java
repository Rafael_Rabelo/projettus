package br.com.projettus.dao;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.bean.ProjetoBean;
import br.com.projettus.bean.UsuarioBean;
import br.com.projettus.entity.Membro;
import br.com.projettus.entity.Topico;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class TopicoDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	
	
    public static boolean saveOrUpdate(Topico topico) {     	        
        try {        	
    		em.getTransaction().begin();
    		
        	if(topico.getIdTopico() == null) {
        		ProjetoBean proj = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");        	
            	topico.setProjeto(proj.getProjeto());
            	
            	UsuarioBean usu = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario"); 
            	@SuppressWarnings("unchecked")
    			List<Membro> membros = (List<Membro>) Consultas.buscaPersonalizada("FROM Membro as membro WHERE membro.usuario.idUsuario = " + usu.getUsuario().getIdUsuario(),em);
            	topico.setMembro(membros.get(0));
            	
            	topico.setDataCriacao(new Date(System.currentTimeMillis()));
            	topico.setVisualizacoes((short) 0);
            	
        		em.persist(topico);
    		}else {
    			em.merge(topico);
    		}
        	
            em.getTransaction().commit();
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Topico "+ topico.getTitulo()+" salvo com sucesso!"));
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao cadastrar topico "));
        	System.out.println(e);
            em.getTransaction().rollback();
        }
        return true;
    }
    public static void delete(Topico topico) {         	
        try {   
			em.getTransaction().begin();
			em.remove(topico);
            em.getTransaction().commit();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Topico "+ topico.getTitulo()+" excluído com sucesso!"));
			           
        } catch (Exception e) {
        	System.out.println(e);
            em.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao excluir topico "+ topico.getTitulo()));
        }
    }    
    
	@SuppressWarnings("unchecked")
	public static List<Topico> list() {
		em.clear();
		ProjetoBean proj = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
	    try {        
        	return (List<Topico>) Consultas.buscaPersonalizada("FROM Topico as top WHERE top.projeto.idProjeto = "+ proj.getProjeto().getIdProjeto() ,em);   
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;        
    }  
}
