package br.com.projettus.dao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import br.com.projettus.bean.ProjetoBean;
import br.com.projettus.bean.UsuarioBean;
import br.com.projettus.entity.Equipe;
import br.com.projettus.util.Consultas;
import br.com.projettus.util.HibernateUtil;

public class EquipeDAO {
	private static EntityManager em = HibernateUtil.geteEntityManagerFactory().createEntityManager();
	public static Equipe equipe = new Equipe();	
	
    public static void saveOrUpdate(Equipe equipe) {
    	ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
        try {       
        	
        	if(equipe.getProjeto() == null) {        		        		 
        		equipe.setProjeto(projBean.getProjeto());        		
        	}   
        	
        	equipe.setCodigo(Consultas.buscaPersonalizada("FROM Equipe WHERE idProjeto = " + equipe.getProjeto().getIdProjeto(), em).size());
   	
        	em.getTransaction().begin();
            em.persist(equipe);
            em.getTransaction().commit();
            if(!equipe.getNome().equals("Gerente(s) do Projeto")) {
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Equipe "+ equipe.getNome()+" salva com sucesso!"));
            }
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao salvar equipe "+ equipe.getNome()));
        	System.out.println(e);
            em.getTransaction().rollback();
        }
    }
    public static void delete(Equipe equipe) {
    	if(equipe.getCodigo() != 0) {
	        try {		        		        	
				em.getTransaction().begin();
				em.remove(equipe);
	            em.getTransaction().commit();	
	        } catch (Exception e) {
	            em.getTransaction().rollback();
	        }
        }else {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Não é possível excluir a equipe administradora"));
        }		
    }        
    
    @SuppressWarnings("unchecked")
	public static List<Equipe> list() {
    	em.clear();
    	ProjetoBean projBean = (ProjetoBean) HibernateUtil.RecuperarDaSessao("projeto");
		List<Object[]> equipeObj = new ArrayList<Object[]>();		
        UsuarioBean usuarioBean = (UsuarioBean) HibernateUtil.RecuperarDaSessao("usuario");        
        try {        
        	if(DashboardDAO.isGerentes()) {
        		return (List<Equipe>) Consultas.buscaPersonalizada("FROM Equipe as equipe WHERE equipe.projeto.idProjeto = " + projBean.getProjeto().getIdProjeto() ,em);        		
        	}else {
	        	equipeObj = (List<Object[]>) Consultas.buscaPersonalizada("FROM Equipe as equipe " + 
	            		"LEFT JOIN FETCH Membro as membro  ON equipe.idEquipe = membro.equipe.idEquipe " + 
	            		"WHERE membro.usuario.idUsuario = " + usuarioBean.getUsuario().getIdUsuario() + " AND equipe.projeto.idProjeto = " + projBean.getProjeto().getIdProjeto(),em);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        List<Equipe> equipes = new ArrayList<Equipe>();
        
        for(Object[] obj : equipeObj) {
        	equipes.add((Equipe) obj[0]);
        }        
        
        return equipes;
    }
}
