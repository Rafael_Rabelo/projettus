package br.com.projettus.util;

import br.com.projettus.entity.Topico;

public class Forum {
	
	private Topico top;
	private String user;
	private String data;
	private int countMsg;
	private boolean javi;
	
	public Topico getTop() {
		return top;
	}

	public void setTop(Topico top) {
		this.top= top;
	}		
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getCountMsg() {
		return countMsg;
	}
	public void setCountMsg(int countMsg) {
		this.countMsg = countMsg;
	}

	public boolean isJavi() {
		return javi;
	}

	public void setJavi(boolean javi) {
		this.javi = javi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + countMsg;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + (javi ? 1231 : 1237);
		result = prime * result + ((top == null) ? 0 : top.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Forum other = (Forum) obj;
		if (countMsg != other.countMsg)
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (javi != other.javi)
			return false;
		if (top == null) {
			if (other.top != null)
				return false;
		} else if (!top.equals(other.top))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
} 
