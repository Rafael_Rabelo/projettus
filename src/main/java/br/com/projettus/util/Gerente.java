package br.com.projettus.util;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import br.com.projettus.dao.DashboardDAO;



@ManagedBean(name="gerente")
@SessionScoped
public class Gerente implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8309213742103266689L;
	private boolean isGerente;
	
	@PostConstruct
    public void init(){
		setGerente(DashboardDAO.isGerentes());
	}

	// GETTERS E SETTERS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	
	public boolean isGerente() {
		return isGerente;
	}

	public void setGerente(boolean isGerente) {
		this.isGerente = isGerente;
	}	
}
