package br.com.projettus.util;

import java.util.List;

import br.com.projettus.entity.Equipe;
import br.com.projettus.entity.Usuario;

public class EquipeMembros{
	Equipe equipe;
	List<Usuario> membros;
	
	public List<Usuario> getMembros() {
		return membros;
	}
	public void setMembros(List<Usuario> usuarios) {
		this.membros = usuarios;
	}
	public Equipe getEquipe() {
		return equipe;
	}
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}			
}
