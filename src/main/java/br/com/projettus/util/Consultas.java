package br.com.projettus.util;

import java.util.List;
import javax.persistence.EntityManager;

public class Consultas {
	
    public static List<?> buscaPersonalizada(String query, EntityManager em) {        
        List<?> list = null;
		try {        	
        	list  = em.createQuery(query).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
        
    }
    
    public static List<?> buscaSQL(String query, EntityManager em) {        
        List<?> list = null;        
		try {        	
        	list  = em.createNativeQuery(query).getResultList();        	
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
        
    }
    

    
}
