package br.com.projettus.util;

import br.com.projettus.entity.Atividade;

public class AtivPlus{
	private Atividade ativ;
	private String dataInicio;
	private String dataFim;
	private String prazo;
	private String prioridadeText;
	private String color;
	private String progresso;
	
	public Atividade getAtiv() {
		return ativ;
	}
	public void setAtiv(Atividade ativ) {
		this.ativ = ativ;
	}
	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	public String getPrioridadeText() {
		return prioridadeText;
	}

	public void setPrioridadeText(String prioridadeText) {
		this.prioridadeText = prioridadeText;
	}
	
	public String getPrazo() {
		return prazo;
	}

	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}
	
	public String getcolor() {
		return color;
	}

	public void setcolor(String color) {
		this.color = color;
	}
	public String getProgresso() {
		return progresso;
	}
	public void setProgresso(String progresso) {
		this.progresso = progresso;
	}
}
